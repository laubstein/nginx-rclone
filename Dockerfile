ARG ALPINE_VERSION
FROM alpine:${ALPINE_VERSION}

RUN apk upgrade --no-cache

RUN mkdir -p /var/lib/nginx \
    && addgroup -g 1001 nginx \
    && adduser -h /var/lib/nginx -u 1001 -G nginx -D -s /bin/false nginx \
    && apk --no-cache add nginx openssl bash

RUN rm /var/lib/nginx/html/*

COPY html/ /var/lib/nginx/html/
COPY conf/ /etc/nginx/
COPY rclone/ /var/lib/nginx/rclone/

ENV RC_VERSION=1.58.0
ADD https://downloads.rclone.org/v${RC_VERSION}/rclone-v${RC_VERSION}-linux-amd64.zip /tmp/rclone.zip

RUN unzip /tmp/rclone.zip rclone-v${RC_VERSION}-linux-amd64/rclone -d /tmp \
    && mv /tmp/rclone-v${RC_VERSION}-linux-amd64/rclone /var/lib/nginx/rclone/ \
    && rm -fr /tmp/rclone*

RUN ln -s /var/lib/nginx/html/500.html /var/lib/nginx/html/502.html \
 && ln -s /var/lib/nginx/html/500.html /var/lib/nginx/html/503.html \
 && ln -s /var/lib/nginx/html/500.html /var/lib/nginx/html/504.html \
 && mkdir -p /run/nginx \
 && chown -R nginx:nginx /etc/nginx /var/lib/nginx /var/log/nginx /run

EXPOSE 8080

USER nginx

WORKDIR /var/lib/nginx

CMD ["/var/lib/nginx/rclone/entrypoint.sh"]
