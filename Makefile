HUB=hub.local
IMG=$(HUB)/plataforma/nginx-rclone
TAG=alpine-3.15

RCLONE_CRYPT_PASSWORD=PWD
RCLONE_CRYPT_PASSWORD2=PWD2
RCLONE_S3_SECRET_ACCESS_KEY=SAK
RCLONE_S3_ACCESS_KEY_ID=AKID
RCLONE_S3_ENDPOINT=http://127.0.0.1:8989

.PHONY: build

build:
	@$(MAKE) steps STEP=buildp

steps:
	$(MAKE) $(STEP) TAG=$(TAG) ALPINE_VERSION=3.15

buildp:
	docker build --build-arg ALPINE_VERSION=$(ALPINE_VERSION) -t $(IMG):$(TAG) .

run:
	@docker ps -a -q --filter name=nginx-rclone | wc -l | grep 1 > /dev/null && (echo "=> container nginx-rclone ja criado, removendo" && docker rm -f nginx-rclone > /dev/null) || echo "=> container nginx-rclone não está no ar"
	docker run -p 8080:8080 \
	       -e "RCLONE_S3_ENDPOINT=$(RCLONE_S3_ENDPOINT)" \
           --name nginx-rclone \
           $(IMG):$(TAG)

run-full:
	@docker ps -a -q --filter name=nginx-rclone | wc -l | grep 1 > /dev/null && (echo "=> container nginx-rclone ja criado, removendo" && docker rm -f nginx-rclone > /dev/null) || echo "=> container nginx-rclone não está no ar"
	docker run -p 8080:8080 \
           -e "RCLONE_S3_ENDPOINT=$(RCLONE_S3_ENDPOINT)" \
		   -e "RCLONE_CRYPT_PASSWORD=$(RCLONE_CRYPT_PASSWORD)" \
           -e "RCLONE_CRYPT_PASSWORD2=$(RCLONE_CRYPT_PASSWORD2)" \
           -e "RCLONE_S3_SECRET_ACCESS_KEY=$(RCLONE_S3_SECRET_ACCESS_KEY)" \
           -e "RCLONE_S3_ACCESS_KEY_ID=$(RCLONE_S3_ACCESS_KEY_ID)" \
           --name nginx-rclone \
           $(IMG):$(TAG)
