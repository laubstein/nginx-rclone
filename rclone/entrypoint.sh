#!/bin/bash

cd /var/lib/nginx/rclone

nohup ./rclone -vvv --rc-serve --rc-no-auth --rc-enable-metrics --rc-addr=127.0.0.1:8081 --rc-web-gui-no-open-browser rcd &!

exec nginx -g 'daemon off;'
