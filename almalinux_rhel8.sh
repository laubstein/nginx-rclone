#!/bin/bash
# Script para emular a mesma configuração, feita utilizando docker, em uma máquina com almalinux/rocky linux/rhel8.

set -e

if [ "$EUID" -ne 0 ]
  then echo "Por favor, rode como root"
  exit
fi

SELINUX_DISABLED=$(getenforce | grep Enforcing > /dev/null; echo $?)

#
# NGINX
#
if [ $SELINUX_DISABLED = 0 ]
then
    echo "SELINUX STATUS: ATIVADO"
else
    echo "SELINUX STATUS: DESATIVADO"
fi

echo "Instalando NGINX e UNZIP"
yum install -y nginx unzip > /dev/null

echo "Definindo /etc/nginx/default.d/rclone.conf"
cat << EOF > /etc/nginx/default.d/rclone.conf
location / {
    proxy_pass http://127.0.0.1:8081/;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
}
EOF

echo 'Removendo "location /" de  /etc/nginx/nginx.conf'
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.original
cat /etc/nginx/nginx.conf.original | tr '\n' '@' | sed -r 's%location / \{@\s+\}%%' | tr '@' '\n' > /etc/nginx/nginx.conf

if [ $SELINUX_DISABLED -eq 0 ]
then
    echo "Ajustando selinux para permitir nginx com proxy reverso"
    setsebool -P httpd_can_network_connect 1
fi

echo "Ativando e iniciando nginx no systemctl"
systemctl enable nginx
systemctl start nginx

# ############################
# RCLONE
# ############################
RC_VERSION=1.58.0
echo "Baixando e configurando rclone ${RC_VERSION}"
curl -s -L https://downloads.rclone.org/v${RC_VERSION}/rclone-v${RC_VERSION}-linux-amd64.zip -o /tmp/rclone.zip

mkdir -p /var/lib/nginx/rclone

echo "Gerando configuração inicial em /var/lib/nginx/rclone/rclone.conf"
cat << EOF > /var/lib/nginx/rclone/rclone.conf
[ceph]
type = s3
provider = Ceph
env_auth = false
acl = bucket-owner-full-control

[ceph-criptografado]
type = crypt
remote = ceph:
filename_encryption = standard
directory_name_encryption = true

[local]
type = local
EOF

echo "Ajustando permissões para /var/lib/nginx/rclone"
chown -R nginx:nginx /var/lib/nginx/rclone

echo "Descompactando rclone para /usr/sbin/rclone"
unzip /tmp/rclone.zip rclone-v${RC_VERSION}-linux-amd64/rclone -d /tmp
mv /tmp/rclone-v${RC_VERSION}-linux-amd64/rclone /usr/sbin/rclone
rm -fr /tmp/rclone*

if [ $SELINUX_DISABLED -eq 0 ]
then
    echo "Ajustando labels para /usr/sbin/rclone"
    restorecon -v /usr/sbin/rclone
fi

echo "Criando rclone.service"
cat << EOF > /etc/systemd/system/rclone.service
# /etc/systemd/system/rclone.service
[Unit]
Description=Rclone
AssertPathIsDirectory=/var/lib/nginx/rclone
After=network-online.target
Before=nginx.service
Wants=network-online.target systemd-networkd-wait-online.service
Requires=nginx.service

[Service]
Type=simple
WorkingDirectory=/var/lib/nginx/rclone
ExecStart=/usr/sbin/rclone \
        --config /var/lib/nginx/rclone/rclone.conf \
        --rc-serve \
        --rc-no-auth \
        --rc-enable-metrics \
        --rc-addr=127.0.0.1:8081 \
        --rc-web-gui-no-open-browser \
        --log-level INFO \
        rcd
Restart=always
RestartSec=10

KillMode=mixed
KillSignal=SIGTERM
TimeoutStopSec=5s

PrivateTmp=true
PrivateDevices=true
ProtectHome=true
ProtectSystem=full
ReadWriteDirectories=/var/lib/nginx/rclone

CapabilityBoundingSet=CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_BIND_SERVICE
NoNewPrivileges=true

[Install]
WantedBy=default.target
EOF

echo "Ativando e iniciando rclone no systemctl"
systemctl daemon-reload
systemctl enable rclone
systemctl start rclone
systemctl status rclone

echo "FIM"
