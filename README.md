# nginx-rclone

Container configurado com nginx e rclone no modo "[remote control](https://rclone.org/rc/)".

# para subir

```bash
make build run RCLONE_S3_ENDPOINT=https://...
```

Após o container estar no ar, acesso http://127.0.0.1:8080 para visualizar a interface web. Nela são listadas todos os backends configurados em [rclone.conf](./rclone/rclone.conf).

O acesso ao backend criptografado só funcionará se o container subir com as credenciais. Utilize `make run-full RCLONE_S3_ENDPOINT=https://... RCLONE_CRYPT_PASSWORD=... RCLONE_CRYPT_PASSWORD2=... RCLONE_S3_SECRET_ACCESS_KEY=... RCLONE_S3_ACCESS_KEY_ID=...`

# modo read only

Para subir o container deixando o acesso somente leitura, substitua o arquivo [nginx.conf](./conf/nginx.conf) pelo arquivo [nginx-read-only.conf](./conf/nginx-read-only.conf) e suba o container novamente.


# envio de arquivos local para um remoto

O rclone possui uma operação de [uploadfile](https://rclone.org/rc/#operations-uploadfile), mas ainda não está claro como fazer uso desta operação. Como alternativa, é possível utilizar uma das seguintes operações:

- `operations/copyfile`: esta operação copia um arquivo de um backend para o outro. Exemplo de chamada:

```
curl -X POST \
     -H "Content-type: application/json" \
     -d '{"dstFs":"local:/tmp","dstRemote":"rclone2","srcFs":"local:/var/lib/nginx/rclone","srcRemote":"rclone"}' \
     http://127.0.0.1:8080/operations/copyfile
```
